
@extends('layouts.common')

@section('title','Company Introduction')



@section('content')

    <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <h3>Company Introduction</h3>

            </header>
            <div class="row">

                <div class="col-xl-6 col-lg-6">

                    <div class="about-col">


                        <h2><span>KNOW ABOUT GERMAN CHEMICALS LIMITED</span></h2>

                        <h5>THE TRUSTED TEXTILE CHEMICAL</h5>

                        <p class="text-justify">Welcome to&nbsp;German Chemicals LTD. is
                            one of the biggest 100% export oriented Textile Auxiliaries Chemicals industry located
                            in Dhaka Export Processing Zone (DEPZ), Ganakbari Savar, Dhaka, Bangladesh, established
                            in 2004 Under Bangladesh Export Processing Zone Authority (BEPZA).

                            <br>
                            <br>
                            This profile is envisages the
                            establishment of a plant for the production of fabric softener in flakes and peril form
                            with a capacity of 50 metric tons per day. The major use of fabric softener in textile
                            and dyeing industry as finishing auxiliaries.

                            <br>
                            <br>
                            The country’s requirement of fabric softener
                            is largely met through import. The present (2014) demand for fabric softener is
                            estimated at more than 22,000 metric tons per month. The demand for the product is
                            projected to reach 5,000 metric tons by the year of 2015.

                            <br>
                            <br>

                            The principal raw material required are fatty acid
                            distillate including other solid and essential acid can be obtain locally while the
                            other raw materials have to be imported.


                            <br>
                            <br>
                            The project cost is estimated as BDT 396.66 million. It
                            will be financed with a debt to equity ratio of 33:67. Thus, equity requirement for the
                            project is BDT 265.74 million. BDT 246.74 million out total equity has already been
                            invested for land purchase, project design, feasibility report preparation and company
                            formation. Total debt requirement of the projects is BDT 130.93 Million.

                            <br>

                            The project can create employment for 59 persons. The
                            establishment of such factory will have a foreign exchange earnings and saving effect to
                            the country by substituting the current imports. The project also create backward
                            linkage with the chemical manufacturing sub sector and also generates income for the
                            government in terms of wage earning of a plant for the production of chemicals.
                        </p>


                    </div>

                </div>

                <div class="col-xl-6 col-lg-6">

                    <div class="about-col about-img">


                        <img src="/img/company/factory.jpg"
                             class="img-responsive img-thumbnail">


                    </div>

                </div>

            </div>

        </div>
    </section>

@endsection
