@extends('layouts.common')

@section('title','MfChairman')



@section('content')

    <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <h3>Message from Chairman</h3>

            </header>

            <div class="row">
                <div class="col-sm-3">

                    <img width="100%" height="232"
                         src="/img/Badiul-Alam-Chairman.png"
                         class="img-thumbnail" alt="">
                </div>
                <div class="col-sm-9">

                    <p class="text-justify">Having started a venture 13 years ago, today I am privileged to be a
                        founding member of one of the most competitive chemical company with emphasis on efficiency in
                        operations, reliability for customers and trust on sustainable development.For us,
                        sustainability means aligning economic success with environmental and social responsibility. In
                        the last few years the German Chemicals Limited faced a series of prolonged crises. These crises
                        allowed us to gain invaluable experience and guided us to newer avenues.</p>
                    <p class="text-justify">I would like to thank our employees, customers, suppliers and other
                        stakeholders, for their dedication and support. We are committed to improving the efficiencies
                        in our operations and differentiating ourselves in the market-place through customer focussed
                        innovation in product and services, so as to build a stronger and sustainable future for our
                        Company and our associates.</p>

                    <img class=""
                         src="/img/MD-sir-sign.png" alt=""
                         width="150px">


                    <p><strong>Chairman<br>
                            German Chemicals Limited</strong></p>
                    <p></p>

                </div>

            </div>
        </div>
    </section>

@endsection